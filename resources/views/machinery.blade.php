@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/machinery.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-machine.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Our Machinery</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>
    </div>
</div>
    
@endsection

@section('content')

<div class="layout-box">
    <div class="layout-section">
        <div class="title-box">
            <h2 class="title-section">Lorem Ipsum Dolor <span>Sit Amet Proin Gravida Vel Avet</span></h2>
            <p class="bg1">Machinery</p>
        </div>
        <div class="desc-box">
            <div>
                <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente itaque non hic mollitia, numquam, maxime natus porro at harum dignissimos officia facilis aperiam iure delectus quibusdam maiores culpa consectetur ipsum.</p>
                <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente itaque non hic mollitia, numquam, maxime natus porro at harum dignissimos officia facilis aperiam iure delectus quibusdam maiores culpa consectetur ipsum.</p>
            </div>
            <div>
                <p class="desc">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius, ipsum! Fuga error odio eos accusamus quasi optio voluptate maxime, autem itaque quod culpa atque porro! Nobis unde magni sequi qui?</p>
                <p class="desc">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius, ipsum! Fuga error odio eos accusamus quasi optio voluptate maxime, autem itaque quod culpa atque porro! Nobis unde magni sequi qui?</p>
            </div>
        </div>
    </div>
</div>

<div class="image-box">
    <div class="img-wrapper">
        <img src="asset/machine1.jpg" alt="">
    </div>
    <div class="img-wrapper">
        <img src="asset/machine2.jpg" alt="">
    </div>
    <div class="line2"></div>
</div>

<div class="slider-box uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">
    <div class="bg-box">
        <img class="bg2" src="asset/bg-service-details.jpg" alt="">
    </div>

    <ul class="uk-slider-items uk-grid">
        <li class="slider-section uk-width-3-5">
            <div class="desc-box uk-flex uk-flex-row">
                <div class="image-box">
                    <img class="img-post" src="asset/about1.jpg" alt="">
                </div>
                <div class="detail-box uk-flex uk-flex-column">
                    <div class="title-box">
                        <h1 class="title-section">Laptop Machine</h1>
                        <img class="line1" src="asset/line2.svg" alt="">                    
                    </div>
                    <div class="desc-section uk-flex uk-flex-column">
                        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur at ad accusantium inventore, blanditiis illo sed, quidem distinctio, eveniet odit quasi facere nisi laborum iste neque doloremque dignissimos ipsa impedit!</p>
                        <p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ratione laudantium perspiciatis magni? Illo in animi nostrum consequuntur possimus optio.</p>
                    </div>
                    <div class="arrow-box uk-flex uk-flex-row uk-flex-middle uk-flex-center">
                        <img src="asset/left-arrow-grey.svg" uk-slider-item="previous" alt="">
                        <div class="divider"></div>
                        <img src="asset/right arrow-grey.svg" uk-slider-item="next" alt="">
                    </div>
                </div>
            </div>
        </li>
        <li class="slider-section uk-width-3-5">
            <div class="desc-box uk-flex uk-flex-row">
                <div class="image-box">
                    <img class="img-post" src="asset/about1.jpg" alt="">
                </div>
                <div class="detail-box uk-flex uk-flex-column">
                    <div class="title-box">
                        <h1 class="title-section">Laptop Machine</h1>
                        <img class="line1" src="asset/line2.svg" alt="">                    
                    </div>
                    <div class="desc-section uk-flex uk-flex-column">
                        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur at ad accusantium inventore, blanditiis illo sed, quidem distinctio, eveniet odit quasi facere nisi laborum iste neque doloremque dignissimos ipsa impedit!</p>
                        <p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ratione laudantium perspiciatis magni? Illo in animi nostrum consequuntur possimus optio.</p>
                    </div>
                    <div class="arrow-box uk-flex uk-flex-row uk-flex-middle uk-flex-center">
                        <img src="asset/left-arrow-grey.svg" uk-slider-item="previous" alt="">
                        <div class="divider"></div>
                        <img src="asset/right arrow-grey.svg" uk-slider-item="next" alt="">
                    </div>
                </div>
            </div>
        </li>
        <li class="slider-section uk-width-3-5">
            <div class="desc-box uk-flex uk-flex-row">
                <div class="image-box">
                    <img class="img-post" src="asset/about1.jpg" alt="">
                </div>
                <div class="detail-box uk-flex uk-flex-column">
                    <div class="title-box">
                        <h1 class="title-section">Laptop Machine</h1>
                        <img class="line1" src="asset/line2.svg" alt="">                    
                    </div>
                    <div class="desc-section uk-flex uk-flex-column">
                        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur at ad accusantium inventore, blanditiis illo sed, quidem distinctio, eveniet odit quasi facere nisi laborum iste neque doloremque dignissimos ipsa impedit!</p>
                        <p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil ratione laudantium perspiciatis magni? Illo in animi nostrum consequuntur possimus optio.</p>
                    </div>
                    <div class="arrow-box uk-flex uk-flex-row uk-flex-middle uk-flex-center">
                        <img src="asset/left-arrow-grey.svg" uk-slider-item="previous" alt="">
                        <div class="divider"></div>
                        <img src="asset/right arrow-grey.svg" uk-slider-item="next" alt="">
                    </div>
                </div>
            </div>
        </li>
    </ul>

</div>

@include('layout.question')

@endsection
