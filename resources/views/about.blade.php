@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/about.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-news.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">About Our Company</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

        {{-- <div class="play-box uk-flex uk-flex-center">
            <img src="asset/icon-play.svg" alt="">
        </div> --}}
    </div>
</div>
    
@endsection

@section('content')

<div class="titled-box">
    <div class="image-box">
        <img src="asset/about1.jpg" alt="">
    </div>
    <div class="detail-box">
        <div class="title-box">
            <h3 class="title-section">Solution Through Tailor <span>Made - Innovations</span></h3>
            <img class="line1" src="asset/home-line1.png" alt="">            
        </div>
        <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis facilis accusamus minus itaque consequuntur reprehenderit fugit laudantium repudiandae debitis alias.</p>
        <p class="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ex maxime saepe doloremque, a tempore rem amet provident necessitatibus! Quis, quisquam!</p>
    </div>
</div>

<div class="quote-box">
    <div class="detail-box">
        <p>Our Company is built on people - those who work for us, and those we do business with</p>
        <p class="petik1">"</p>
        <p class="petik2">"</p>
    </div>
    <div class="image-box">
        <img src="asset/news2.jpg" alt="">
        <div class="line3"></div>
    </div>
</div>

<div class="vision-box">
    <div class="card-box">
        @foreach ($cardAbout as $cardAbouts)
        <div class="card-section uk-flex">
            <div class="image-box uk-flex uk-flex-center uk-flex-middle uk-flex-none">
                <img src={{$cardAbouts['img']}} alt="">
            </div>
            <div class="detail-box">
                <p class="title">{{$cardAbouts['title']}}</p>
                <p class="desc">{{$cardAbouts['desc']}}</p>
            </div>
        </div>
        @endforeach
    </div>
    <img class="bg" src="asset/about2.jpg" alt="">
</div>

<div class="avet-box">
    <div class="image-box">
        <img src="asset/about1.jpg" alt="">
    </div>
    <div class="detail-box">
        <div class="title-box">
            <h3 class="title-section">Solution Through Tailor <span>Made - Innovations</span></h3>
            <img class="line1" src="asset/home-line1.png" alt="">                    
        </div>
        <ul>
            <li class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut autem, praesentium reiciendis blanditiis in facere aperiam nobis excepturi saepe ad eveniet eius? Vitae, reiciendis facere unde praesentium eum itaque possimus. Eius ex quasi nobis animi voluptatibus doloribus eveniet cum omnis.</li>
            <li class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nesciunt voluptas iure reprehenderit quasi repellendus, quam tempore ratione aut veritatis consequatur exercitationem reiciendis error laborum officiis corrupti ea ex fugit consequuntur, accusantium modi aliquam suscipit expedita. Qui magni sapiente ducimus?</li>
        </ul>
    </div>
</div>

<div class="mission-box">
    <div class="image-box uk-position-relative">
        <img src="asset/about2.jpg" alt="">
        <div class="play-box uk-flex uk-flex-center uk-position-center">
            <img src="asset/icon-play.svg" alt="">
        </div>
    </div>
    <div class="visi-box">
        <div class="title-box">
            <h3 class="title-section">Our <span>Vision</span></h3>
            <p class="bg1">Vision</p>
        </div>
        <p class="desc">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores veritatis, quidem autem vitae sint accusamus quaerat similique dicta suscipit nihil soluta illum, ullam consectetur numquam nemo voluptates mollitia iure ut laborum nesciunt minima sed. Qui obcaecati quisquam sed molestias accusantium!</p>
    </div>
    <div class="misi-box">
        <div class="title-box">
            <h3 class="title-section">Our <span>Mission</span></h3>
            <p class="bg1">Mission</p>
        </div>
        <p class="desc" >Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam tenetur, maxime obcaecati nemo magni iusto, nisi quasi ut doloribus qui vitae placeat eveniet? Cupiditate nisi numquam quasi obcaecati, ut modi dolorum hic totam minus veniam quibusdam sunt inventore. Ex, suscipit!</p>
    </div>
</div>

@include('layout.question')

<div class="line2"></div>

@endsection
