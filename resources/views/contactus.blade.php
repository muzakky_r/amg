@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/contactus.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/contact1.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Contact Us</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

    </div>
</div>
    
@endsection

@section('content')

<div class="map-box">
    <img src="asset/map.jpg" alt="">
    {{-- <div class="mapouter">
        <div class="gmap_canvas">
            <iframe class="map" id="gmap_canvas" src="https://maps.google.com/maps?q=bxc&t=&z=13&ie=UTF8&iwloc=&output=embed" 
            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
        </iframe>
        </div>
    </div> --}}
</div>

<div class="form-box">
    <form action="">
        <input class="first-name" type="text" placeholder="Your First Name">
        <input class="last-name" type="text" placeholder="Your Last Name">
        <input class="phone" type="text" placeholder="Your Phone Number">
        <input class="email" type="text" placeholder="Your Email">
        <textarea class="textarea" name="" placeholder="Your Message" cols="30" rows="10"></textarea>
        <div class="detail-box">
            <div class="address">
                <p>Salembaran Raya, Jl. Cilampe Pergudangan 2000, Blok A No 9-10 Kosambi, Tangerang</p>
            </div>
            <div class="contact">
                <p>T, +62 21 559 313 96</p>
                <p>F, +62 21 559 313 96</p>
                <p>E, admin@amgprinting.com</p>
            </div>
        </div>
        <button class="uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
            Send 
            <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
            <img class="arrow2" src="asset/arrow-right.svg" alt="">
        </button>
    </form>
</div>

@endsection
