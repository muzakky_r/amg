@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/factory.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-factory.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Our Factory</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>
    </div>
</div>
    
@endsection

@section('content')

<div class="layout-box">
    <div class="layout-section">
        <div class="title-box">
            <h2 class="title-section">Lorem Ipsum Dolor <span>Sit Amet Proin Gravida Vel Avet</span></h2>
            <p class="bg1">Factory</p>
        </div>
        <div class="desc-box">
            <div>
                <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente itaque non hic mollitia, numquam, maxime natus porro at harum dignissimos officia facilis aperiam iure delectus quibusdam maiores culpa consectetur ipsum.</p>
            </div>
            <div>
                <p class="desc">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius, ipsum! Fuga error odio eos accusamus quasi optio voluptate maxime, autem itaque quod culpa atque porro! Nobis unde magni sequi qui?</p>
            </div>
        </div>
    </div>
</div>

<div class="image-box">
    @foreach ($factoryImg as $factoryImgs)
    <img src={{$factoryImgs}} alt="">
    @endforeach
</div>
    
@include('layout.question')

@endsection
