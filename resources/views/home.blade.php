@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/home.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-home.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Solution Through Tailor Made - Innovations</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

        <div class="play-box uk-flex uk-flex-center">
            <img src="asset/icon-play.svg" alt="">
        </div>
    </div>
</div>
    
@endsection

@section('content')
    <div class="box about-box">
        <div class="image-box" uk-scrollspy="cls: uk-animation-slide-left-small">
            <img class="portrait" src="asset/home1.jpg" alt="">
        </div>
        <div class="detail-box ux-flex ux-flex-column" uk-scrollspy="cls: uk-animation-slide-right-small">
            <div class="title-box">
                <h2 class="title-section">About <span>Our Company</span></h2>
                <img class="line1" src="asset/home-line1.png" alt="">
                <p class="bg1">About</p>
            </div>
            <div class="detail-section">
                <h5 class="sub-title">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur, magni?</h5>
                <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
    
                <button  class="uk-button uk-button-default">
                    Read More
                    <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                    <img class="arrow2" src="asset/arrow-right.svg" alt="">
                </button>
            </div>
        </div>
    </div>

    <div class="box offer-box">
        <div class="title-box uk-flex uk-flex-bottom" uk-scrollspy="cls: uk-animation-slide-left-small">
            <h2 class="title-section">What <span>We Offer</span></h2>
            <p class="bg1">Services</p>
            <img class="line1" src="asset/home-line1.png" alt="">            
        </div>
        <div class="image-box uk-flex uk-flex-right" uk-scrollspy="cls: uk-animation-slide-right-small">
            <img class="landscape" src="asset/home2.jpg" alt="">
            <div class="line2"></div>
            <div class="line3"></div>
        </div>
        <div class="card-wrapper uk-flex uk-flex-between" uk-scrollspy="cls: uk-animation-slide-top-small">
            <img onclick="prevOffer()" class="arrow arrow-left" src="asset/arrow-left.svg" alt="">
            <div class="card-box">
                <div class="card-slider">
                    @foreach ($cardHome as $cardHomes)
                    <div class="card-section uk-flex">
                        <img src={{$cardHomes['img']}} alt="">                    
                        <div class="card-detail">
                            <p class="title">{{$cardHomes['title']}}</p>
                            <p class="desc">{{$cardHomes['desc']}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <img class="arrow arrow-right" onclick="nextOffer()" src="asset/arrow-right.svg" alt="">
        </div>
    </div>

    <div class="box machinery-box">
        <div class="top-box" uk-scrollspy="cls: uk-animation-slide-top-small">
            <div class="detail-box">
                <div class="title-box">
                    <h1 class="title-section">Our <span>Machinery</span></h1>
                    <p class="bg1">Machinery</p>
                    <img class="line1" src="asset/home-line1.png" alt="">
                </div>
                <p class="desc">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.
                    accusantium doloremque laudantium, 
                </p>      
            </div>
            <div class="image-box">
                <img src="asset/factory4.jpg" alt="">
            </div>
        </div>
        <div class="bottom-box" uk-scrollspy="cls: uk-animation-slide-bottom-small">
            <div class="image-box">
                <img src="asset/home4.jpg" alt="">
            </div>
            <div class="slider-box">
                <div class="img-box machinery-slider">
                    <img class="machinery-section" src="asset/bg-news.jpg" alt="">
                    <img class="machinery-section" src="asset/about1.jpg" alt="">
                    <img class="machinery-section" src="asset/service-details2.png" alt="">
                    <img class="machinery-section" src="asset/bg-news.jpg" alt=""> 
                    <img class="machinery-section" src="asset/about1.jpg" alt="">
                </div>
                <div class="arrow-box uk-flex uk-flex-row">
                    <img onclick="prevMachinery()" class="machinery-left" src="asset/arrow-left.svg" alt="">
                    <img onclick="nextMachinery()" class="machinery-right" src="asset/arrow-right.svg" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="box client-box">
        <div class="title-box uk-flex uk-flex-bottom" uk-scrollspy="cls: uk-animation-slide-left-small">
            <h2 class="title-section">Our <span>Clientele</span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">     
            <p class="bg1">Clients</p>                   
        </div>
        <div class="detail-box" uk-scrollspy="cls: uk-animation-slide-right-small">
            <img class="bg" src="asset/bg-service-details.jpg" alt="">
            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.
                    accusantium doloremque laudantium, </p>
            <div class="logo-box">
                <img src="asset/client4.png" alt="">
                <img src="asset/client3.png" alt="">
                <img src="asset/client2.png" alt="">
                <img src="asset/client1.png" alt="">
            </div>
        </div>
    </div>

    <div class="box contact-box">
        <img class="bg" src="asset/machine2.jpg" alt="">
        <div class="image-box">
            <p class="bg1">Contact</p>
            <img src="asset/contact2.jpg" alt="">
        </div>
        <div class="title-box uk-flex uk-flex-bottom">
            <h2 class="title-section">Get In <span>Touch!</span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">                 
        </div>
        <div class="form-box">
            <form action="">
                <input type="text" placeholder="Your Name">
                <input type="email" placeholder="Your Email Address">
                <textarea cols="30" rows="10" placeholder="Message" ></textarea>
                <button class="uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                    Send 
                    <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                    <img class="arrow2" src="asset/arrow-right.svg" alt="">
                </button>
            </form>
        </div>

        <div class="detail-box">
            <div class="address">
                <p>Salembaran Raya, Jl. Cilampe Pergudangan 2000, Blok A No 9-10 Kosambi, Tangerang</p>
            </div>
            <div class="contact">
                <p><span uk-icon="icon: phone"></span> +62 21 559 313 96</p>
                <p><span uk-icon="icon: print"></span> +62 21 559 313 96</p>
                <p><span uk-icon="icon: mail"></span> admin@amgprinting.com</p>
            </div>
        </div>
    </div>


    <script>
        var totalMachinerySlide = document.querySelectorAll(".machinery-section").length + 1;
        var clickMachinerySlide = totalMachinerySlide - 3;
        var sliderMachinery = document.querySelector(".machinery-slider");
        var m = 1;
        var n = 1;


        var totalOfferSlide = document.querySelectorAll(".card-section").length + 1;
        var clickOfferSlide = totalOfferSlide - 3;
        var sliderOffer = document.querySelector(".card-slider");
        var i = 1;
        var j = 1;

        function nextOffer(){
            document.querySelector(".arrow-left").style.opacity = 1;            
            if(i < clickOfferSlide){
                var rumus = 'translateX(calc((' + i + ' * -30%) - (' + i + ' * 4.5vw)))';
                sliderOffer.style.transform = rumus;
                i = i + 1;
                j = j - 1;
            }
            if(i >= clickOfferSlide){
                document.querySelector(".arrow-right").style.opacity = 0;
            }
        }

        function prevOffer(){
            document.querySelector(".arrow-right").style.opacity = 1;            
            if(i > 1){
                var rumus = 'translateX(calc((' + j + ' * 30%) + (' + j + ' * 4.5vw)))';
                sliderOffer.style.transform = rumus;
                i = i - 1;
                j = j + 1;
            }
            if(i == 1){
                document.querySelector(".arrow-left").style.opacity = 0;
            }
        }

        
        function nextMachinery(){
            document.querySelector(".machinery-left").style.opacity = 1;            
            if(m < clickMachinerySlide){
                var rumus = 'translateX(calc((' + m + ' * -32%) - (' + m + ' * 1vw)))';
                sliderMachinery.style.transform = rumus;
                m = m + 1;
                n = n - 1;
            }
            if(m >= clickMachinerySlide){
                document.querySelector(".machinery-right").style.opacity = 0;
            }
        }

        function prevMachinery(){
            document.querySelector(".machinery-right").style.opacity = 1;            
            if(m > 1){
                var rumus = 'translateX(calc((' + n + ' * 32%) + (' + n + ' * 1vw)))';
                sliderMachinery.style.transform = rumus;
                m = m - 1;
                n = n + 1;
            }
            if(m == 1){
                document.querySelector(".machinery-left").style.opacity = 0;
            }
        }

    </script>
@endsection