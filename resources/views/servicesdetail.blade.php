@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/servicesdetail.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-service-details.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Special</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

        {{-- <div class="play-box uk-flex uk-flex-center">
            <img src="asset/icon-play.svg" alt="">
        </div> --}}
    </div>
</div>
    
@endsection

@section('content')
<div class="box about-box">
    <div class="detail-box ux-flex ux-flex-column">
        <div class="title-box">
            <h2 class="title-section">We Offer the services <span>to help you work better</span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">                        
        </div>
        <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
        <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
    </div>
    <div class="image-box">
        <img class="landscape" src="asset/service-details2.png" alt="">
        <div class="line2"></div>
    </div>
</div>

<div class="box flock-box">
    <div class="image-box">
        <img class="portrait" src="asset/service-detail1.jpg" alt="">
    </div>
    <div class="detail-box ux-flex ux-flex-column">
        <div class="title-box">
            <h2 class="title-section">Special <span></span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">                        
        </div>
        <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
        <p class="desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
    </div>
</div>

<div class="other-box">
    <div class="post-box">
        <div class="image-box">
            <img src="asset/machine1.jpg" alt="">
        </div>
        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga aspernatur unde libero assumenda. Natus ab tempore quam maxime expedita ipsam. Fuga aspernatur unde libero assumenda. Natus ab tempore quam maxime expedita ipsam.</p>
    </div>
    <div class="post-box">
        <div class="image-box">
            <img src="asset/service-detail3.jpg" alt="">
            <div class="line2"></div>            
        </div>
        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga aspernatur unde libero assumenda. Natus ab tempore quam maxime expedita ipsam. Fuga aspernatur unde libero assumenda. Natus ab tempore quam maxime expedita ipsam.</p>
    </div>
</div>

<div class="single-img-box">
    <img src="asset/bg-machine.jpg" alt="">
</div>
@endsection
