@extends('layout.master')

@section('title', 'Homes')

@section('css')
    <link rel="stylesheet" href="css/newsdetail.css">

@section('navbar')
    @include('layout.navbar')
@endsection

@section('content')

<div class="content-box uk-flex uk-flex-column uk-flex-middle">
    <div class="image-box">
        <img src="asset/newsdetail1.jpg" alt="">
    </div>
    <div class="news-box">
        <div class="news-section">
            <div class="date-box">
                <p>27 Agustus 2019</p>
            </div>
            <div class="desc-box">
                <p class="title">Manchester United</p>
                <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui nesciunt illum reprehenderit accusamus voluptas, velit possimus molestias adipisci explicabo quo, officiis non esse hic fugiat ut, tenetur commodi dolor. Eligendi pariatur rerum sed iste totam? Hic sequi libero, facere impedit suscipit fuga in voluptatem rem ipsa incidunt ut autem neque esse similique inventore? Ratione accusamus necessitatibus, fugiat facere provident dicta nostrum maiores, reiciendis cupiditate a suscipit corrupti dolores alias iure quod ea ipsum. Veniam minima ratione et incidunt. Culpa voluptate eos, iure autem voluptatum totam explicabo in vero est quasi facilis! Atque a earum natus expedita tempore? Ullam, alias fugiat.</p>
            </div>
        </div>
        <div class="footer-news-box uk-flex uk-flex-row uk-flex-between">
            <button class="amg-button uk-button uk-button-default">
                Back
                <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                <img class="arrow2" src="asset/arrow-right.svg" alt="">
            </button>
            <div class="socmed-box uk-flex uk-flex-row uk-flex-between uk-flex-middle">
                <p>Share: </p>
                <div class="icon-box">
                    <img src="asset/icon-fb.svg" alt="">
                    <img src="asset/icon-twitter.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
