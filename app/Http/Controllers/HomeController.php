<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $cardHome =[
            [
                "img"=>"asset/discharge.png",
                "title"=>"Discharge",
                "desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            ],
            [
                "img"=>"asset/plastisol.png",
                "title"=>"Plastisol",
                "desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            ],
            [
                "img"=>"asset/rubber.png",
                "title"=>"Rubber",
                "desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            ],
            [
                "img"=>"asset/special.png",
                "title"=>"Special",
                "desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            ],
            [
                "img"=>"asset/sublimation.png",
                "title"=>"Sublimation",
                "desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            ],
        ];

        return view('home', compact('cardHome'));

    }
}
