<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index(){

        $cardService =[
            [
                "img"=>"asset/discharge.png",
                "title"=>"Discharge",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ],
            [
                "img"=>"asset/rubber.png",
                "title"=>"Rubber",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ],
            [
                "img"=>"asset/sublimation.png",
                "title"=>"sublimation",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ],
            [
                "img"=>"asset/plastisol.png",
                "title"=>"plastisol",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ],
            [
                "img"=>"asset/special.png",
                "title"=>"special",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ],
        ];

        return view('services', compact('cardService'));
    }
}
