const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .extract(['vue'])
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/master.scss', 'public/css')
    .sass('resources/sass/home.scss', 'public/css')
    .sass('resources/sass/about.scss', 'public/css')    
    .sass('resources/sass/clients.scss', 'public/css')    
    .sass('resources/sass/contactus.scss', 'public/css')    
    .sass('resources/sass/construction.scss', 'public/css')    
    .sass('resources/sass/factory.scss', 'public/css')    
    .sass('resources/sass/getinfo.scss', 'public/css')    
    .sass('resources/sass/machinery.scss', 'public/css')    
    .sass('resources/sass/news.scss', 'public/css')    
    .sass('resources/sass/newsdetail.scss', 'public/css')    
    .sass('resources/sass/notfound.scss', 'public/css')    
    .sass('resources/sass/ourapproach.scss', 'public/css')    
    .sass('resources/sass/search.scss', 'public/css')    
    .sass('resources/sass/services.scss', 'public/css')    
    .sass('resources/sass/servicesdetail.scss', 'public/css')    
    ;
